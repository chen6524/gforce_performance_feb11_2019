//
// File:        CYFISNP_NODE_Var.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes

////////////////////////
// definitions
typedef unsigned char byte;
#define NULL    (byte*)0

////////////////////////
// internal vars
byte CYFISNP_NODE_XactConfig=0;     // current value of XACT_CFG_ADDR
// the value of Force End State bit in this variable is indeterminate
byte CYFISNP_NODE_RestingState=0;   // current state of Rx core while not attempting a transaction
// An exception to this is in receive mode (due to a workaround for a hardware issue).
// If the user wants the end state to be SLEEP, The RF core actually goes into IDLE,
// and EndReceive does the work of putting the RF core to sleep.  In this case,
// CYFISNP_NODE_RestingState will be SLEEP even though the actual state may temporarily be IDLE.
// In all cases, the values of the bits in this variable not corresponding to the state field
// of XACT_CFG_ADR are indeterminate.

byte CYFISNP_NODE_State=0;          // current state of Rx/Tx
byte CYFISNP_NODE_Temp[4]={0};

byte *CYFISNP_NODE_WipPtr=NULL;     // working pointer
byte CYFISNP_NODE_WipLen=0;         // working len
byte *CYFISNP_NODE_Ptr=NULL;        // user specified pointer 
byte CYFISNP_NODE_Len=0;            // user specified len

byte CYFISNP_NODE_BytesRead=0;
byte CYFISNP_NODE_TxCtrlShadow=0;

////////////////////////
// external vars

//////////////////////////////////////////////////////////////////////////////////
//
