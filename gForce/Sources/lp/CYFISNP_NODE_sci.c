//
// File:        sci.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"

////////////////////////
// definitions

////////////////////////
// internal vars

////////////////////////
// external vars

#ifdef fDEBUG
//////////////////////////////////////////////////////////////////////////////////
//

//
char kbhit(void)
{
#if (PCB_TYPE==PCB_DEMOJM)
    return SCI1S1_RDRF;     // Receive Data Register Full Flag
#elif (PCB_TYPE==PCB_gFT)
    return SCI2S1_RDRF;     // Receive Data Register Full Flag
#endif  // PCB_TYPE
}

//
byte getch(void)
{
#if (PCB_TYPE==PCB_DEMOJM)
    return (byte)SCI1D;
#elif (PCB_TYPE==PCB_gFT)
    return (byte)SCI2D;
#endif  // PCB_TYPE
}

//
void putch(byte data)
{
    // wait until Tx buffer is empty
#if (PCB_TYPE==PCB_DEMOJM)
    while( !SCI1S1_TDRE )   // Transmit Data Register Empty Flag
        ;

    SCI1D = data;
#elif (PCB_TYPE==PCB_gFT)
    while( !SCI2S1_TDRE )   // Transmit Data Register Empty Flag
        ;

    SCI2D = data;
#endif  // PCB_TYPE
}

//
void outstr(byte *msg)
{
    while(*msg)
        putch(*msg++);
}

//
void outhex(byte data)
{
    static const byte hex[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

    putch(hex[data >> 4]);
    putch(hex[data & 0xf]);
}

//
void outdec(byte data)
{
    byte temp=data;

    if( data >= 100 ) {
        putch('0'+(temp/100));
        temp %= 100;
    }
    if( data >= 10 ) {
        putch('0'+(temp/10));
        temp %= 10;
    }
    putch('0'+temp);
}

void outnibble(byte data)
{
    data &= 0x0F;

    if( data > 9 )
        data += ('A' - 10);
    else
        data += '0';

    putch(data);
}

#endif  // fDEBUG
