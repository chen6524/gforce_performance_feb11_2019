//
// File:        CYFISNP_NODE_Regs.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

#ifndef CYFISNP_NODE_Regs_h_
#define CYFISNP_NODE_Regs_h_

// -------------------------------
// Channel register
// -------------------------------
#define CYFISNP_NODE_CHANNEL_ADR                                       0x00
#define CYFISNP_NODE_CHANNEL_RST                                       0x48
#define CYFISNP_NODE_CHANNEL_MSK                                       0x7F

#define CYFISNP_NODE_CHANNEL_MAX                                       0x62
#define CYFISNP_NODE_CHANNEL_MIN                                       0x00
#define CYFISNP_NODE_CHANNEL_2P498_GHZ                                 0x62
#define CYFISNP_NODE_CHANNEL_2P4_GHZ                                   0x00


// -------------------------------
// TX Length register
// -------------------------------
#define CYFISNP_NODE_TX_LENGTH_ADR                                     0x01
#define CYFISNP_NODE_TX_LENGTH_RST                                     0x00
#define CYFISNP_NODE_TX_LENGTH_MSK                                     0xFF


// -------------------------------
// TX Control register
// -------------------------------
#define CYFISNP_NODE_TX_CTRL_ADR                                       0x02
#define CYFISNP_NODE_TX_CTRL_RST                                       0x03

// See TX_IRQ for remaining bit position definitions

// TX_CTRL bit masks
#define CYFISNP_NODE_TX_GO                                             0x80
#define CYFISNP_NODE_TX_CLR                                            0x40


// -------------------------------
// TX Configuration register
// -------------------------------
#define CYFISNP_NODE_TX_CFG_ADR                                        0x03
#define CYFISNP_NODE_TX_CFG_RST                                        0x05

// separate bit field masks
#define CYFISNP_NODE_TX_DATCODE_LEN_MSK                                0x20
#define CYFISNP_NODE_TX_DATMODE_MSK                                    0x18
#define CYFISNP_NODE_PA_VAL_MSK                                        0x07

// DATCODE_LEN register masks
#define CYFISNP_NODE_DATCODE_LEN_64                                    0x20
#define CYFISNP_NODE_DATCODE_LEN_32                                    0x00

// DATMODE register masks
#define CYFISNP_NODE_DATMODE_1MBPS                                     0x00
#define CYFISNP_NODE_DATMODE_8DR                                       0x08

// PA_SET register masks
#define CYFISNP_NODE_PA_N30_DBM                                        0x00
#define CYFISNP_NODE_PA_N25_DBM                                        0x01
#define CYFISNP_NODE_PA_N20_DBM                                        0x02
#define CYFISNP_NODE_PA_N15_DBM                                        0x03
#define CYFISNP_NODE_PA_N10_DBM                                        0x04
#define CYFISNP_NODE_PA_N5_DBM                                         0x05
#define CYFISNP_NODE_PA_0_DBM                                          0x06
#define CYFISNP_NODE_PA_4_DBM                                          0x07


// -------------------------------
// TX IRQ Status register
// -------------------------------
#define CYFISNP_NODE_TX_IRQ_STATUS_ADR                                 0x04

// TX_IRQ bit masks
#define CYFISNP_NODE_XS_IRQ                                            0x80
#define CYFISNP_NODE_LV_IRQ                                            0x40
#define CYFISNP_NODE_TXB15_IRQ                                         0x20
#define CYFISNP_NODE_TXB8_IRQ                                          0x10
#define CYFISNP_NODE_TXB0_IRQ                                          0x08
#define CYFISNP_NODE_TXBERR_IRQ                                        0x04
#define CYFISNP_NODE_TXC_IRQ                                           0x02
#define CYFISNP_NODE_TXE_IRQ                                           0x01


// -------------------------------
// RX Control register
// -------------------------------
#define CYFISNP_NODE_RX_CTRL_ADR                                       0x05
#define CYFISNP_NODE_RX_CTRL_RST                                       0x07

// See RX_IRQ register for bit positions definitions also used for this register

// RX_CTRL bit masks
#define CYFISNP_NODE_RX_GO                                             0x80


// -------------------------------
// RX Configuration register
// -------------------------------
#define CYFISNP_NODE_RX_CFG_ADR                                        0x06
#define CYFISNP_NODE_RX_CFG_RST                                        0x92

#define CYFISNP_NODE_AUTO_AGC_EN                                       0x80
#define CYFISNP_NODE_LNA_EN                                            0x40
#define CYFISNP_NODE_ATT_EN                                            0x20
#define CYFISNP_NODE_HI                                                0x10
#define CYFISNP_NODE_LO                                                0x00
#define CYFISNP_NODE_FASTTURN_EN                                       0x08
#define CYFISNP_NODE_RXOW_EN                                           0x02
#define CYFISNP_NODE_VLD_EN                                            0x01


// -------------------------------
// RX IRQ register
// -------------------------------
#define CYFISNP_NODE_RX_IRQ_STATUS_ADR                                 0x07
// There is no default value for this register.

// RX_IRQ bit masks
#define CYFISNP_NODE_RXOW_IRQ                                          0x80
#define CYFISNP_NODE_SOPDET_IRQ                                        0x40
#define CYFISNP_NODE_RXB16_IRQ                                         0x20
#define CYFISNP_NODE_RXB8_IRQ                                          0x10
#define CYFISNP_NODE_RXB1_IRQ                                          0x08
#define CYFISNP_NODE_RXBERR_IRQ                                        0x04
#define CYFISNP_NODE_RXC_IRQ                                           0x02
#define CYFISNP_NODE_RXE_IRQ                                           0x01


// -------------------------------
// RX Status register
// -------------------------------
#define CYFISNP_NODE_RX_STATUS_ADR                                     0x08
// There is no default value for this register.

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_RX_ACK                                            0x80
#define CYFISNP_NODE_RX_PKTERR                                         0x40
#define CYFISNP_NODE_RX_EOPERR                                         0x20
#define CYFISNP_NODE_RX_CRC0                                           0x10
#define CYFISNP_NODE_RX_BAD_CRC                                        0x08
#define CYFISNP_NODE_RX_DATCODE_LEN                                    0x04
#define CYFISNP_NODE_RX_DATMODE_MSK                                    0x03


// -------------------------------
// RX Count register
// -------------------------------
#define CYFISNP_NODE_RX_COUNT_ADR                                      0x09
#define CYFISNP_NODE_RX_COUNT_RST                                      0x00
#define CYFISNP_NODE_RX_COUNT_MSK                                      0xFF


// -------------------------------
// RX Length Field register
// -------------------------------
#define CYFISNP_NODE_RX_LENGTH_ADR                                     0x0A
#define CYFISNP_NODE_RX_LENGTH_RST                                     0x00
#define CYFISNP_NODE_RX_LENGTH_MSK                                     0xFF


// -------------------------------
// Power Control register
// -------------------------------
#define CYFISNP_NODE_PWR_CTRL_ADR                                      0x0B
#define CYFISNP_NODE_PWR_CTRL_RST                                      0xA0

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_PMU_EN                                            0x80
#define CYFISNP_NODE_LV_IRQ_EN                                         0x40
#define CYFISNP_NODE_PMU_SEN                                           0x20 // DEPRECIATED
#define CYFISNP_NODE_PMU_MODE_FORCE                                    0x20
#define CYFISNP_NODE_PFET_OFF                                          0x10
#define CYFISNP_NODE_LV_IRQ_TH_MSK                                     0x0C
#define CYFISNP_NODE_PMU_OUTV_MSK                                      0x03

// LV_IRQ_TH values
#define CYFISNP_NODE_LV_IRQ_TH_1P8_V                                   0x0C
#define CYFISNP_NODE_LV_IRQ_TH_2P0_V                                   0x08
#define CYFISNP_NODE_LV_IRQ_TH_2P2_V                                   0x04
#define CYFISNP_NODE_LV_IRQ_TH_PMU_OUTV                                0x00

// PMU_OUTV values
#define CYFISNP_NODE_PMU_OUTV_2P4                                      0x03
#define CYFISNP_NODE_PMU_OUTV_2P5                                      0x02
#define CYFISNP_NODE_PMU_OUTV_2P6                                      0x01
#define CYFISNP_NODE_PMU_OUTV_2P7                                      0x00


// -------------------------------
// Crystal Control register
// -------------------------------
#define CYFISNP_NODE_XTAL_CTRL_ADR                                     0x0C
#define CYFISNP_NODE_XTAL_CTRL_RST                                     0x04

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_XOUT_FNC_MSK                                      0xC0
#define CYFISNP_NODE_XS_IRQ_EN                                         0x20
#define CYFISNP_NODE_XOUT_FREQ_MSK                                     0x07

// XOUT_FNC values
#define CYFISNP_NODE_XOUT_FNC_XOUT_FREQ                                0x00
#define CYFISNP_NODE_XOUT_FNC_PA_N                                     0x40
#define CYFISNP_NODE_XOUT_FNC_RAD_STREAM                               0x80
#define CYFISNP_NODE_XOUT_FNC_GPIO                                     0xC0

// XOUT_FREQ values
#define CYFISNP_NODE_XOUT_FREQ_12MHZ                                   0x00
#define CYFISNP_NODE_XOUT_FREQ_6MHZ                                    0x01
#define CYFISNP_NODE_XOUT_FREQ_3MHZ                                    0x02
#define CYFISNP_NODE_XOUT_FREQ_1P5MHZ                                  0x03
#define CYFISNP_NODE_XOUT_FREQ_P75MHZ                                  0x04

// -------------------------------
// Analog Control register
// -------------------------------
#define CYFISNP_NODE_ANALOG_CTRL_ADR                                   0x39
#define CYFISNP_NODE_ALLSLOW                                           0x01


// -------------------------------
// I/O Configuration register
// -------------------------------
#define CYFISNP_NODE_IO_CFG_ADR                                        0x0D
#define CYFISNP_NODE_IO_CFG_RST                                        0x00
#define CYFISNP_NODE_IO_CFG_MSK                                        0xFF

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_IRQ_OD                                            0x80
#define CYFISNP_NODE_IRQ_POL                                           0x40
#define CYFISNP_NODE_MISO_OD                                           0x20
#define CYFISNP_NODE_XOUT_OD                                           0x10
#define CYFISNP_NODE_PACTL_OD                                          0x08
#define CYFISNP_NODE_PACTL_GPIO                                        0x04
#define CYFISNP_NODE_SPI_3_PIN                                         0x02
#define CYFISNP_NODE_IRQ_GPIO                                          0x01


// -------------------------------
// GPIO Control register
// -------------------------------
#define CYFISNP_NODE_GPIO_CTRL_ADR                                     0x0E
#define CYFISNP_NODE_GPIO_CTRL_RST                                     0x00
#define CYFISNP_NODE_GPIO_CTRL_MSK                                     0xF0

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_XOUT_OP                                           0x80
#define CYFISNP_NODE_MISO_OP                                           0x40
#define CYFISNP_NODE_PACTL_OP                                          0x20
#define CYFISNP_NODE_IRQ_OP                                            0x10
#define CYFISNP_NODE_XOUT_IP                                           0x08
#define CYFISNP_NODE_MISO_IP                                           0x04
#define CYFISNP_NODE_PACTL_IP                                          0x02
#define CYFISNP_NODE_IRQ_IP                                            0x01


// -------------------------------
// Transaction Configuration register
// -------------------------------
#define CYFISNP_NODE_XACT_CFG_ADR                                      0x0F
#define CYFISNP_NODE_XACT_CFG_RST                                      0x80

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_ACK_EN                                            0x80
#define CYFISNP_NODE_FRC_END_STATE                                     0x20
#define CYFISNP_NODE_END_STATE_MSK                                     0x1C
#define CYFISNP_NODE_ACK_TO_MSK                                        0x03

// END_STATE field values
#define CYFISNP_NODE_END_STATE_SLEEP                                   0x00
#define CYFISNP_NODE_END_STATE_IDLE                                    0x04
#define CYFISNP_NODE_END_STATE_TXSYNTH                                 0x08
#define CYFISNP_NODE_END_STATE_RXSYNTH                                 0x0C
#define CYFISNP_NODE_END_STATE_RX                                      0x10

// ACK_TO field values
#define CYFISNP_NODE_ACK_TO_4X                                         0x00
#define CYFISNP_NODE_ACK_TO_8X                                         0x01
#define CYFISNP_NODE_ACK_TO_12X                                        0x02
#define CYFISNP_NODE_ACK_TO_15X                                        0x03


// -------------------------------
// Framing Configuration register
// -------------------------------
#define CYFISNP_NODE_FRAMING_CFG_ADR                                   0x10
#define CYFISNP_NODE_FRAMING_CFG_RST                                   0xA5

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_SOP_EN                                            0x80
#define CYFISNP_NODE_SOP_LEN_64                                        0x40
#define CYFISNP_NODE_SOP_LEN_32                                        0x00
#define CYFISNP_NODE_LEN_EN                                            0x20
#define CYFISNP_NODE_SOP_THRESH_MSK                                    0x1F


// -------------------------------
// Data Threshold 32 register
// -------------------------------
#define CYFISNP_NODE_DATA32_THOLD_ADR                                  0x11
#define CYFISNP_NODE_DAT32_THRESH_RST                                  0x04
#define CYFISNP_NODE_DAT32_THRESH_MSK                                  0x0F


// -------------------------------
// Data Threshold 64 register
// -------------------------------
#define CYFISNP_NODE_DATA64_THOLD_ADR                                  0x12
#define CYFISNP_NODE_DAT64_THRESH_RST                                  0x0A
#define CYFISNP_NODE_DAT64_THRESH_MSK                                  0x1F


// -------------------------------
// RSSI register
// -------------------------------
#define CYFISNP_NODE_RSSI_ADR                                          0x13
#define CYFISNP_NODE_RSSI_RST                                          0x20

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_SOP_RSSI                                          0x80
#define CYFISNP_NODE_LNA_STATE                                         0x20
#define CYFISNP_NODE_RSSI_LVL_MSK                                      0x1F


// -------------------------------
// EOP Control register
// -------------------------------
#define CYFISNP_NODE_EOP_CTRL_ADR                                      0x14
#define CYFISNP_NODE_EOP_CTRL_RST                                      0xA4

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_HINT_EN                                           0x80
#define CYFISNP_NODE_HINT_EOP_MSK                                      0x70
#define CYFISNP_NODE_EOP_MSK                                           0x0F


// -------------------------------
// CRC Seed registers
// -------------------------------
#define CYFISNP_NODE_CRC_SEED_LSB_ADR                                  0x15
#define CYFISNP_NODE_CRC_SEED_MSB_ADR                                  0x16
#define CYFISNP_NODE_CRC_SEED_LSB_RST                                  0x00
#define CYFISNP_NODE_CRC_SEED_MSB_RST                                  0x00

// CRC related values
// USB CRC-16
#define CYFISNP_NODE_CRC_POLY_MSB                                      0x80
#define CYFISNP_NODE_CRC_POLY_LSB                                      0x05
#define CYFISNP_NODE_CRC_RESI_MSB                                      0x80
#define CYFISNP_NODE_CRC_RESI_LSB                                      0x0D


// -------------------------------
// TX CRC Calculated registers
// -------------------------------
#define CYFISNP_NODE_TX_CRC_LSB_ADR                                    0x17
#define CYFISNP_NODE_TX_CRC_MSB_ADR                                    0x18


// -------------------------------
// RX CRC Field registers
// -------------------------------
#define CYFISNP_NODE_RX_CRC_LSB_ADR                                    0x19
#define CYFISNP_NODE_RX_CRC_MSB_ADR                                    0x1A
#define CYFISNP_NODE_RX_CRC_LSB_RST                                    0xFF
#define CYFISNP_NODE_RX_CRC_MSB_RST                                    0xFF


// -------------------------------
// Synth Offset registers
// -------------------------------
#define CYFISNP_NODE_TX_OFFSET_LSB_ADR                                 0x1B
#define CYFISNP_NODE_TX_OFFSET_MSB_ADR                                 0x1C
#define CYFISNP_NODE_TX_OFFSET_LSB_RST                                 0x00
#define CYFISNP_NODE_TX_OFFSET_MSB_RST                                 0x00

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_STRIM_MSB_MSK                                     0x0F
#define CYFISNP_NODE_STRIM_LSB_MSK                                     0xFF


// -------------------------------
// Mode Override register
// -------------------------------
#define CYFISNP_NODE_MODE_OVERRIDE_ADR                                 0x1D
#define CYFISNP_NODE_MODE_OVERRIDE_RST                                 0x00

#define CYFISNP_NODE_FRC_AWAKE                                         0x03
#define CYFISNP_NODE_FRC_AWAKE_OFF_1                                   0x01
#define CYFISNP_NODE_FRC_AWAKE_OFF_2                                   0x00

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_DIS_AUTO_SEN                                      0x80
#define CYFISNP_NODE_SEN_TXRXB                                         0x40
#define CYFISNP_NODE_FRC_SEN                                           0x20
#define CYFISNP_NODE_FRC_AWAKE_MSK                                     0x18
#define CYFISNP_NODE_MODE_OVRD_FRC_AWAKE                               0x18
#define CYFISNP_NODE_MODE_OVRD_FRC_AWAKE_OFF_1                         0x08
#define CYFISNP_NODE_MODE_OVRD_FRC_AWAKE_OFF_2                         0x00
#define CYFISNP_NODE_RST                                               0x01
#define CYFISNP_NODE_FRC_PA                                            0x02


// -------------------------------
// RX Override register
// -------------------------------
#define CYFISNP_NODE_RX_OVERRIDE_ADR                                   0x1E
#define CYFISNP_NODE_RX_OVERRIDE_RST                                   0x00

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_ACK_RX                                            0x80
#define CYFISNP_NODE_EXTEND_RX_TX                                      0x40
#define CYFISNP_NODE_MAN_RXACK                                         0x20
#define CYFISNP_NODE_FRC_RXDR                                          0x10
#define CYFISNP_NODE_DIS_CRC0                                          0x08
#define CYFISNP_NODE_DIS_RXCRC                                         0x04
#define CYFISNP_NODE_ACE                                               0x02


// -------------------------------
// TX Override register
// -------------------------------
#define CYFISNP_NODE_TX_OVERRIDE_ADR                                   0x1F
#define CYFISNP_NODE_TX_OVERRIDE_RST                                   0x00

// single flag bits & multi-bit-field masks
#define CYFISNP_NODE_ACK_TX_SEN                                        0x80
#define CYFISNP_NODE_FRC_PREAMBLE                                      0x40
#define CYFISNP_NODE_DIS_TX_RETRANS                                    0x20
#define CYFISNP_NODE_MAN_TXACK                                         0x10
#define CYFISNP_NODE_OVRRD_ACK                                         0x08
#define CYFISNP_NODE_DIS_TXCRC                                         0x04
#define CYFISNP_NODE_CO                                                0x02
#define CYFISNP_NODE_TXINV                                             0x01


//------------------------------------------------------------------------------
//      File Function Detail
//------------------------------------------------------------------------------

// -------------------------------
// TX Buffer - 16 bytes
// -------------------------------
#define CYFISNP_NODE_TX_BUFFER_ADR                                     0x20


// -------------------------------
// RX Buffer - 16 bytes
// -------------------------------
#define CYFISNP_NODE_RX_BUFFER_ADR                                     0x21


// -------------------------------
// Framing Code - 8 bytes
// -------------------------------
#define CYFISNP_NODE_SOP_CODE_ADR                                      0x22

// CODESTORE_REG_SOF_RST        64'h17_ff_9e_21_36_90_c7_82
#define CYFISNP_NODE_CODESTORE_BYTE7_SOF_RST                           0x17
#define CYFISNP_NODE_CODESTORE_BYTE6_SOF_RST                           0xFF
#define CYFISNP_NODE_CODESTORE_BYTE5_SOF_RST                           0x9E
#define CYFISNP_NODE_CODESTORE_BYTE4_SOF_RST                           0x21
#define CYFISNP_NODE_CODESTORE_BYTE3_SOF_RST                           0x36
#define CYFISNP_NODE_CODESTORE_BYTE2_SOF_RST                           0x90
#define CYFISNP_NODE_CODESTORE_BYTE1_SOF_RST                           0xC7
#define CYFISNP_NODE_CODESTORE_BYTE0_SOF_RST                           0x82


// -------------------------------
// Data Code - 16 bytes
// -------------------------------
#define CYFISNP_NODE_DATA_CODE_ADR                                     0x23

// CODESTORE_REG_DCODE0_RST            64'h01_2B_F1_DB_01_32_BE_6F
#define CYFISNP_NODE_CODESTORE_BYTE7_DCODE0_RST                        0x01
#define CYFISNP_NODE_CODESTORE_BYTE6_DCODE0_RST                        0x2B
#define CYFISNP_NODE_CODESTORE_BYTE5_DCODE0_RST                        0xF1
#define CYFISNP_NODE_CODESTORE_BYTE4_DCODE0_RST                        0xDB
#define CYFISNP_NODE_CODESTORE_BYTE3_DCODE0_RST                        0x01
#define CYFISNP_NODE_CODESTORE_BYTE2_DCODE0_RST                        0x32
#define CYFISNP_NODE_CODESTORE_BYTE1_DCODE0_RST                        0xBE
#define CYFISNP_NODE_CODESTORE_BYTE0_DCODE0_RST                        0x6F

// CODESTORE_REG_DCODE1_RST            64'h02_F9_93_97_02_FA_5C_E3
#define CYFISNP_NODE_CODESTORE_BYTE7_DCODE1_RST                        0x02
#define CYFISNP_NODE_CODESTORE_BYTE6_DCODE1_RST                        0xF9
#define CYFISNP_NODE_CODESTORE_BYTE5_DCODE1_RST                        0x93
#define CYFISNP_NODE_CODESTORE_BYTE4_DCODE1_RST                        0x97
#define CYFISNP_NODE_CODESTORE_BYTE3_DCODE1_RST                        0x02
#define CYFISNP_NODE_CODESTORE_BYTE2_DCODE1_RST                        0xFA
#define CYFISNP_NODE_CODESTORE_BYTE1_DCODE1_RST                        0x5C
#define CYFISNP_NODE_CODESTORE_BYTE0_DCODE1_RST                        0xE3


// -------------------------------
// Preamble - 3 bytes
// -------------------------------
#define CYFISNP_NODE_PREAMBLE_ADR                                      0x24

#define CYFISNP_NODE_PREAMBLE_CODE_MSB_RST                             0x33
#define CYFISNP_NODE_PREAMBLE_CODE_LSB_RST                             0x33
#define CYFISNP_NODE_PREAMBLE_LEN_RST                                  0x02


// -------------------------------
// Laser Fuses - 8 bytes (2 hidden)
// -------------------------------
#define CYFISNP_NODE_MFG_ID_ADR                                        0x25


// -------------------------------
// XTAL Startup Delay
// -------------------------------
#define CYFISNP_NODE_XTAL_CFG_ADR                                      0x26
#define CYFISNP_NODE_XTAL_CFG_RST                                      0x00

// -------------------------------
// Clock Override
// -------------------------------
#define CYFISNP_NODE_CLK_OVERRIDE_ADR                                  0x27
#define CYFISNP_NODE_CLK_OVERRIDE_RST                                  0x00

#define CYFISNP_NODE_RXF                                               0x02


// -------------------------------
// Clock Enable
// -------------------------------
#define CYFISNP_NODE_CLK_EN_ADR                                        0x28
#define CYFISNP_NODE_CLK_EN_RST                                        0x00

#define CYFISNP_NODE_RXF                                               0x02


// -------------------------------
// Receiver Abort
// -------------------------------
#define CYFISNP_NODE_RX_ABORT_ADR                                      0x29
#define CYFISNP_NODE_RX_ABORT_RST                                      0x00

#define CYFISNP_NODE_ABORT_EN                                          0x20


// -------------------------------
// Auto Calibration Time
// -------------------------------
#define CYFISNP_NODE_AUTO_CAL_TIME_ADR                                 0x32
#define CYFISNP_NODE_AUTO_CAL_TIME_RST                                 0x0C

#define CYFISNP_NODE_AUTO_CAL_TIME_MAX                                 0x3C


// -------------------------------
// Auto Calibration Offset
// -------------------------------
#define CYFISNP_NODE_AUTO_CAL_OFFSET_ADR                               0x35
#define CYFISNP_NODE_AUTO_CAL_OFFSET_RST                               0x00

#define CYFISNP_NODE_AUTO_CAL_OFFSET_MINUS_4                           0x14

// ---------------------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------------------

//
// SPI Interface definitions:
//
#define CYFISNP_NODE_mSPI_ADDRESS   0x3F
#define CYFISNP_NODE_bSPI_WRITE     0x80
#define CYFISNP_NODE_bSPI_AUTO_INC  0x40

//
// RADIO_STATE definitions:
//
#define CYFISNP_NODE_IDLE        0x00
#define CYFISNP_NODE_RX          0x80
#define CYFISNP_NODE_TX          0x20
#define CYFISNP_NODE_SOP         CYFISNP_NODE_SOPDET_IRQ
#define CYFISNP_NODE_DATA        CYFISNP_NODE_RXB1_IRQ
#define CYFISNP_NODE_COMPLETE    CYFISNP_NODE_RXC_IRQ        // Code assumes these two bits are BOTH
#define CYFISNP_NODE_ERROR       CYFISNP_NODE_RXE_IRQ        //  in the RX_CTRL_ADR register.

// Defaults for TX_CFG_REG (32_8DR:250kbps, 64_8DR=125kbps)
#define CYFISNP_NODE_DEF_DATA_RATE    (CYFISNP_NODE_DATCODE_LEN_32|CYFISNP_NODE_DATMODE_8DR) // 0x08
#define CYFISNP_NODE_DEF_TX_POWER      CYFISNP_NODE_PA_4_DBM        // 0x07

// Defaults for XACT_CFG_REG
#define CYFISNP_NODE_DEF_END_STATE     CYFISNP_NODE_END_STATE_SLEEP // 0x00
#define CYFISNP_NODE_DEF_ACK_ENABLE    CYFISNP_NODE_ACK_EN          // 0x80
#define CYFISNP_NODE_DEF_ACK_TIMEOUT   CYFISNP_NODE_ACK_TO_8X       // 0x01

// Defaults for FRAMING_CFG_REG
#define CYFISNP_NODE_DEF_SOP_EN        CYFISNP_NODE_SOP_EN          // 0x80
#define CYFISNP_NODE_DEF_SOP_LEN       CYFISNP_NODE_SOP_LEN_32      // 0x00
#define CYFISNP_NODE_DEF_LEN_EN        CYFISNP_NODE_LEN_EN          // 0x20

#if (CYFISNP_NODE_DEF_SOP_LEN)
    #define CYFISNP_NODE_DEF_SOP_TSH   0x0E     // for SOP64
#else
    #define CYFISNP_NODE_DEF_SOP_TSH   0x04     // for SOP32
#endif

// Default Thresholds
#define CYFISNP_NODE_DEF_32_THOLD      0x05
#define CYFISNP_NODE_DEF_64_THOLD      0x0E

#define CYFISNP_NODE_DEF_PREAMBLE_CNT  0x1

//
// Return value for a RadioAbort completed successfully. Otherwise the return value
//  is the length of the packet received (without error).
//
#define CYFISNP_NODE_ABORT_SUCCESS     0xFF


#define CYFISNP_NODE_CHAN_MIN          0x00
#define CYFISNP_NODE_CHAN_MAX          0x4d

#define CYFISNP_NODE_MAX_CHANNELS      78 // 6 sets * 13 ch = 78 ch total


#define CYFISNP_NODE_SIZEOF_MID        4

//--------------------------------------
// Type Declarations
//--------------------------------------

typedef unsigned char RADIO_LENGTH;
typedef unsigned char RADIO_REG_ADDR;
typedef void         *RADIO_BUFFER_PTR;
typedef const char   *RADIO_CONST_PTR;
typedef unsigned char RADIO_RSSI;
typedef unsigned char RADIO_FRAME_CONFIG;
typedef unsigned char RADIO_TX_STATUS;
typedef unsigned char RADIO_STATE;
typedef unsigned char XACT_CONFIG;
typedef unsigned char TX_CONFIG;
typedef unsigned char RADIO_RX_STATUS;

#endif // CYFISNP_NODE_Regs_h_
