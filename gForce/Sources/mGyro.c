/********************************************************************************
*  
*
* History:
*
* 04/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "mGyro.h"
#include "LSM6DS3.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mHid.h"
#include "rtc.h"
#include <stdlib.h>
#include "kbi.h"

#include "bcd_common.h"                        // Added by Jason Chen for time stamp fix, 2015.05.07

GYRO_INFO gyroInfo[ACC_MAX_BD_NUM];

GYRO_STATE gyroState=GYRO_STATE_SOFT_RESET;

volatile byte lgIsrState =0;
//volatile static ACC_DATA accData; 
byte Statup_flag = 3;                          // Added by Jason Chen for first time startup, 2017.03.30

void lgISR (void)
{
   DISABLE_LOWG_INT;
   pwrOffCnt =0;
   if(Statup_flag) 
   {
      Statup_flag--;
      imSleepTimerTime = 75;                   // Added by Jason Chen, 2017.03.30
   } 
   else
   		imSleepTimerTime = IM_SLEEP_TIMER;       // Added by Jason Chen, 2014.11.26
   motionDetected =2;
   return;
   #if 0
   lgIsrState++;
   //adc sample now
    if (!KBI_USB_ON)
    {
  	  //gftInfo.gHGCnt++;
      accHitStat = ACC_HIT_STAT_INIT;
      //if(imState == IM_STATE_SLEEP_WAIT)
	  //  imState = IM_STATE_IDLE;
	  //ADCSC1 = adcChanGame[0] | 0x40;//start adc
	  //ADCSC1_AIEN = 1;
      //testMsS = g_Tick;
      
    }	
   imSleepTimerTime = IM_SLEEP_TIMER;
   DISABLE_LOWG_INT;
   return;
   #endif
}

void gyroInit(void)
{
	  
    SPI_MEMS_Acc_Config( 0);
    SPI_MEMS_Gyro_Config();
  //set stream mode
    //gyro_FifoStream();
}

/* 
	the sample rate for the gyro is 760Hz
*/
#define GYRO_SAMPLE_INTERVAL (1/760)
byte gyroGet(void)
{
   byte i;
   GYRO_INFO *pGyro = pAccNowW->pGyroInfo;
   //there are 32  sets of data 
   for(i = 0; i<32; i++)
   {   		
   		#if ENABLE_ACC_DATA_OUTPUT                                      // Added by Jason Chen for acc test,2014.05.07
   		  //acc_FIFO_SPIGetRawData((byte *)(&(pGyro->fifoData[i])));		
   		#else
   		  //gyro_FIFO_SPIGetRawData((uint8_t*)(&(pGyro->fifoData[i])));   		
   		#endif
   }
   return 1;
}


#define     gyroGetSum(x,y,z) (x*x+y*y+z*z)

byte gyroCalc(void)
{  
  byte i;
  
  //int16_t t1 =0, t2=0, t3 =0;
  //byte which = 0;
  int32_t t = 0, r = 0;
  //int16_t s1 =0, s2=0, s3 =0;
  GYRO_INFO *pGyro = pAccNowR->pGyroInfo;
   //calculate the max acc
  for(i = 1; i<32; i++)
  {  	  

	#if 0
		t1 = (pGyro->fifoData[i].x- pGyro->fifoData[i-1].x);
		s1 = t1;
		if (t1<0){t1 = -t1; }
		
		t2 = (pGyro->fifoData[i].y- pGyro->fifoData[i-1].y);
		s2 = t2;
		if (t2<0) {t2 = -t2;}
		
		t3 = (pGyro->fifoData[i].z- pGyro->fifoData[i-1].z);
		s3 = t3;
		if (t3<0 ) { t3 = -t3;}
		
		t =  t1+t2+t3;
	#endif
        
    #if ENABLE_ACC_DATA_OUTPUT                                      // Added by Jason Chen for acc test,2014.05.07
      t =  gyroGetSum((int32_t)(pGyro->fifoData[i].x >> 4) , (int32_t)(pGyro->fifoData[i].y >> 4) , (int32_t)(pGyro->fifoData[i].z>>4));	    
    #else
      t =  gyroGetSum((int32_t)pGyro->fifoData[i].x , (int32_t)pGyro->fifoData[i].y , (int32_t)pGyro->fifoData[i].z);	    
    #endif
    
		if( t > r )
		{	
			 r =t;
			 pGyro->maxAcc.x = pGyro->fifoData[i].x;///GYRO_SAMPLE_INTERVAL; let do the calculation in PC
             pGyro->maxAcc.y = pGyro->fifoData[i].y;///GYRO_SAMPLE_INTERVAL;
             pGyro->maxAcc.z = pGyro->fifoData[i].z;///GYRO_SAMPLE_INTERVAL;
		}			
		//pAccNowR->pEntry->dataG = pGyro->
   }
  
        //copy data 
		//(void)memcpy(pAccNowR->pEntry->dataG,pGyro->fifoData,sizeof(GYRO_DATA)*GYRO_FIFO_SIZE); //FIX
		pAccNowR->pEntry->gyroActive.gyro.x = pGyro->maxAcc.x;
		pAccNowR->pEntry->gyroActive.gyro.y = pGyro->maxAcc.y;
	    pAccNowR->pEntry->gyroActive.gyro.z = pGyro->maxAcc.z;
	    
   return 1;
   
}

//static LOWG_DATA lgAccData[32];
#define BUFFER_SIZE  30
//static LOWG_DATA lgAccVData[BUFFER_SIZE];                                 // Added by Jason Chen for preventing wrong recording , 2014.05.06 
ACC_DATA lgAccV;
ACC_DATA lgAccVT;

GYRO_DATA lgGyroV;
GYRO_DATA lgGyroVT;

#define NEGATIVE_SIGN =0x8000

static int16_t convertLg(int16_t a)
{
	 int16_t d = a;
	 if(d > 0) 
		 d = (d  >>  4);
	 else
		 d = (int16_t)((((unsigned int)d & (unsigned int)0x7fff)	>>	4) | (unsigned int)0x8000);
	
	 return d;
}


byte lgAccGet(byte mode) {

  // int16_t d;
   //if(acc_SPIReadReg(ACC_STATUS_REG)& 0x08)
   acc_FIFO_SPIGetRawData((byte *)(&lgAccV));
   
   
   
   if(0)//mode)
   {
   		//lgAccV.x += (int16_t)(usrProfile.calLinOffset[0]);
   		//lgAccV.y += (int16_t)(usrProfile.calLinOffset[1]);
   		//lgAccV.z += (int16_t)(usrProfile.calLinOffset[2]);
   }
   //lgAccV.x = convertLg(lgAccV.x);
   //lgAccV.y = convertLg(lgAccV.y);
   //lgAccV.z = convertLg(lgAccV.z);
   
   return 1;
}
#if 0
byte lgGyroGet(void)
{
       
       gyro_FIFO_SPIGetRawData((uint8_t*)(&lgGyroV));  
	   return 1;
}
#endif
byte lgGyroGet(void)
{

   int32_t gyroMax =0;
   int32_t gyroMaxTemp =0;
   byte i;
   GYRO_DATA gyroData; 
   
    
     
   //for(i = 0; i<1; i++)
   {   		  		
   		 gyro_FIFO_SPIGetRawData((uint8_t*)(&lgGyroVT));
		 //gyroMaxTemp = accGetResult((long)lgGyroVT.x,(long)lgGyroVT.y,(long)lgGyroVT.z);	
	    // if(gyroMaxTemp >gyroMax)
	     // {
		//	  gyroMax =  gyroMaxTemp;
		 //     gyroData = lgGyroVT;
		//  }
   		  
		
   }
   //lgGyroVT = gyroData;
   return 1;
}

void lgAccGetData(void)                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 
{
   byte i;
   ACC_DATA mData[32];
   int32_t sumX = 0,sumY = 0,sumZ = 0;
   //there are 32  sets of data 
   
   for(i = 0; i<32; i++)
   {
   		
   		gyro_FIFO_SPIGetRawData((byte *)(&mData[i]));  
		if(i < 16)
		{
		 sumX += mData[i].x;
		 sumY += mData[i].y;
		 sumZ += mData[i].z;
		}
   }
   lgAccV.x = sumX/16;
   lgAccV.y = sumY/16;
   lgAccV.z = sumZ/16;
}

#define THRESHOLD_VALUE_TT                     62500L                 // 250 * 250
byte getMaxThresh(void)                                 // Added by Jason Chen for preventing wrong recording , 2014.05.06 
{
#if USE_INTERRUPT_FILTER
    byte status = 0;
    status = acc_SPIReadReg(ACC_INT1_SRC)&0x60;
    if(status)
       acc_SPIWriteReg(ACC_INT1_CFG,ACC_INT1_CFG_6DIR_MOVE|XYZ_UP_INT_ENABLE|XYZ_DOWN_INT_ENABLE);
    return status;
#else
   byte i;

   int32_t t = 0, r = 0;
     
   //lgAccGetData();   
     
   for(i = 0; i< BUFFER_SIZE; i++)
   {  	          
      acc_FIFO_SPIGetRawData((byte *)(&lgAccV));		
      #if ENABLE_ACC_DATA_OUTPUT                                      // Added by Jason Chen for acc test,2014.05.07
         pAccNowW->pGyroInfo->fifoData[i].x = lgAccV.x;
         pAccNowW->pGyroInfo->fifoData[i].y = lgAccV.y;
         pAccNowW->pGyroInfo->fifoData[i].z = lgAccV.z;
      #endif
      t =  gyroGetSum((int32_t)(lgAccV.x>>4) , (int32_t)(lgAccV.y>>4) , (int32_t)(lgAccV.z>>4));	    
      if( t > r )
    	  r = t;
   }
// t = (thresR * 1000)/12;           // 12mg/digit       +/- 16g configuration
 //t = 250    ;                      // (250 * 12)/1000 = 3.0 g
   t = THRESHOLD_VALUE_TT;           // (250 * 12)/1000 = 3.0 g
   if( r > t)    
     return 1;
   else            
     return 0;
#endif   
}

static byte lgAccCalc(void)
{
     lgAccV.x = lgAccV.x>>4;
     lgAccV.y = lgAccV.y>>4;
     lgAccV.z = lgAccV.z>>4;
    return 1;
}

static byte lgState = LG_STATE_INIT;

void lowGTask(byte usbUnpluged)
{
    static byte cnt = 0;
	if(startCal)
	{
       switch(lgState)
       {
          case LG_STATE_INIT:     
             
              //SPI_MEMS_Acc_Config(1);
              lgAccV.x = 0;
              lgAccV.y = 0;
              lgAccV.z = 0;
              
              lgState = LG_STATE_CAL_START;
              break;
          case LG_STATE_CAL_START:
              
              lgAccGet(0);
              if(cnt++ == 1)
              {
                lgState = LG_STATE_CAL_CAC;
                cnt = 0;
              }
            break;
          case LG_STATE_CAL_CAC:
              //lgAccCalc();
              lgState = LG_STATE_CAL_END;
            break;
          case LG_STATE_CAL_END:
              lgState = LG_STATE_INIT;
              startCal =0;
          break;
        }
    }
	else if (g_SampleFlagL & usbUnpluged)
	{
     (void)lgAccGet(1);
	 //(void)lgGyroGet();	
	 DisableInterrupts;
	  lgAccVT = lgAccV;
	  //lgGyroVT = lgGyroV;
	 EnableInterrupts; 
     g_SampleFlagL = 0;
	}

}

