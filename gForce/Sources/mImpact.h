/******************************************************************************
**
*  
*
* History:
*
* 11/01/2012 -- first version, ver.1.0  H.YAN
* 
*
*******************************************************************************
*/

#ifndef _MIMPACT_H
#define _MIMPACT_H
#include "typedef.h"

#include "rtc.h"
#include "gft.h"
#include "typedef.h"

#include "mGyro.h"

#include "kbi.h"


#include "bcd_common.h"                             // Added by Jason Chen, 2014.05.20
//test 
//extern word testMsS;
//extern word testMsE;


#define ACC_HIT_DATA_PRE 0x1
#define ACC_HIT_DATA_PST 0x2      

#pragma MESSAGE DISABLE C1106 /* WARNING C1106: Non-standard bitfield type */

#define ACC_RESOLUTION_SQR  62500/9 //0.4096


#define IMPACT_ARM_THD        60 //60 g
#define IMPACT_X_THD          10 //41 *0.244
#define IMPACT_Y_THD          10 //41
#define IMPACT_Y_THD          10 //41              

typedef enum 
{
	IM_STATE_NONE,
    IM_STATE_INIT,
    IM_STATE_WAIT, 
    IM_STATE_IDLE,
    IM_STATE_SLEEP_WAIT,
    IM_STATE_SLEEP,
    IM_STATE_WAKE,
    IM_STATE_READ, 
    IM_STATE_CALC,
    IM_STATE_SAVE,
    IM_STATE_SOFT_RESET,
    
    IM_STATE_SENDING_SLEEP_MSG   // Added by Jason Chen for sending Sleep MSG, 2014.01.16
    
}IMPACT_STATE;

#define ACC_GYRO_MAGIC      0x4C

#define ACC_ENTRY_MAGIC     0x48
#define ACC_ENTRY_START     0x49
#define ACC_ENTRY_END       0x4A
#define SESSION_ENTRY_MAGIC 0x4B
#define WAIT_AFTER_RESET      50

#define ACC_HIT_CNT_MAX    20
#define ACC_HIT_PRE_CNT    24
#define ACC_HIT_PST_CNT    96
#define ACC_HIT_DATA_LEN   ACC_HIT_CNT_MAX 

//if the number is 80, the total structure size will be 255 add one dummy 
// it will exactly be one page  TODO 
//100
#define ACC_MAX_DATA_NUMBER  ACC_HIT_DATA_LEN 
#if ( ACC_MAX_DATA_NUMBER > 127)
#error "The Max number exceeded...\n"
#endif

#define GYRO_FIFO_SIZE 32


typedef struct 
{
  uint16_t bucket[12];
  
}NYR_BUCKET;




typedef struct 
{
    byte magic;                                 // 0
  	MY_RTC t;                                   // 1~8
  	byte resvC;
  	#define DEVICE_TYPE_LSM3300 0x1
	#define DEVICE_TYPE_LSM6DS3 0x0
  	byte deviceType;                                  // 9~10
  	
  	byte sid; //session ID                                                // 11
  	byte maxIndex;                              // 12
  	word mag;                                   // 13~14
  	
  	union 
  	{
  	  //gyro data
  	   GYRO_DATA gyro;//max rotation 
  	   uint32_t activeTime;
  	}gyroActive;                                 // 15~20
  	 NYR_BUCKET bucket;                          // 21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,44
   	
	  union
	  {
	    MY_RTC oldRTC[2];	 
	    MOTION_DATA data[ACC_MAX_DATA_NUMBER];    // 12 x 20  = 240
	  }rtcAcc;                                    // 45 + 240 = 285
	  uint32_t iFlag;
	  byte resvB[2];                              // 285 + 2 = 286 = 23 x 13 -12, last two byte of last packet for buckets numbering (counting), Jason Chen, 2016.01.27
} ACC_ENTRY;

typedef struct 
{
      byte magic;                                 // 0
  	  MY_RTC t;                                   // 1~8
  	  word resv;                                  // 9~10
  	  byte sid; //session ID                                                // 11
  	  byte resB;                                  // 12
  	  word resW1;                                   // 13~14
  	  uint32_t activeTime;
  	  byte resvA[2];                            // 15~20       
	  MY_RTC oldRTC[2];	  
	  byte resvC[2];                             
} ACC_SESSION;
//#define BUCKET_RESOLUTION 48L
#define BUCKET_RESOLUTION 31L //31.232

#if 0
#define B_1G ((1000/BUCKET_RESOLUTION)*(1000/BUCKET_RESOLUTION))  
#define B_2G ((2000/BUCKET_RESOLUTION)*(2000/BUCKET_RESOLUTION))   //1736
#define B_3G ((3000/BUCKET_RESOLUTION)*(3000/BUCKET_RESOLUTION))   //3906
#define B_4G ((4000/BUCKET_RESOLUTION)*(4000/BUCKET_RESOLUTION))   //6944
#define B_5G ((5000/BUCKET_RESOLUTION)*(5000/BUCKET_RESOLUTION))   //10850
#define B_6G ((6000/BUCKET_RESOLUTION)*(6000/BUCKET_RESOLUTION))   //15625
#define B_7G ((7000/BUCKET_RESOLUTION)*(7000/BUCKET_RESOLUTION))   //21267
#define B_8G ((8000/BUCKET_RESOLUTION)*(8000/BUCKET_RESOLUTION))   //27778
#define B_9G ((9000/BUCKET_RESOLUTION)* (9000/BUCKET_RESOLUTION))  //35156
#define B_10G ((10000/BUCKET_RESOLUTION)*(10000/BUCKET_RESOLUTION)) //434030
#else
#define B_1G ((1000/BUCKET_RESOLUTION)*(1000/BUCKET_RESOLUTION))  
#define B_2G ((1600/BUCKET_RESOLUTION)*(1600/BUCKET_RESOLUTION))   //1736
#define B_3G ((1800/BUCKET_RESOLUTION)*(1800/BUCKET_RESOLUTION))   //3906
#define B_4G ((2000/BUCKET_RESOLUTION)*(2000/BUCKET_RESOLUTION))   //6944
#define B_5G ((2200/BUCKET_RESOLUTION)*(2200/BUCKET_RESOLUTION))   //10850
#define B_6G ((2400/BUCKET_RESOLUTION)*(2400/BUCKET_RESOLUTION))   //15625
#define B_7G ((2600/BUCKET_RESOLUTION)*(2600/BUCKET_RESOLUTION))   //21267
#define B_8G ((2800/BUCKET_RESOLUTION)*(2800/BUCKET_RESOLUTION))   //27778
#define B_9G ((3000/BUCKET_RESOLUTION)* (3000/BUCKET_RESOLUTION))  //35156
#define B_10G ((35000/BUCKET_RESOLUTION)*(35000/BUCKET_RESOLUTION)) //434030


#endif

#define B_1G_OFFSET   1




#define UNDEFINED 0xFF
typedef struct 
{
    byte magic; 
   
    byte needEraseFlag;  //default 0xff
    byte eraseDoneFlag;  //default 0xff
    
	MY_RTC startTime;  
    MY_RTC endTime;
    
    byte  accBits;
    
    uint32_t startAddr;
    uint32_t endAddr;   
    
    uint32_t cnt;
} SESSION_ENTRY;

#define ACC_ENTRY_HEADER_LENGH sizeof(ACC_ENTRY)-sizeof(MOTION_DATA)*ACC_MAX_DATA_NUMBER  


//#define MHID_ACC_ENTRY_SIZE   (sizeof(ACC_ENTRY)-sizeof(ACC_DATA)*ACC_MAX_DATA_NUMBER-1)
//magic
#define GFT_PROFILE_MAGIC 0x59


#define PROFILE_SID_STAT_MSK 0x80

//alarmMode
#define PROFILE_ALARM_EN_MSK    0x80
#define PROFILE_ALARM_AU_MSK    0x10
#define PROFILE_ALARM_VI_MSK    0x20
#define PROFILE_ALARM_LOCK_MSK  0x40    //return to play interlock
#define PROFILE_ALARM_ON_MSK    0x1

typedef struct 
{
	byte magic; 
	byte GID[6]; 
	byte tAlarm;
	byte tR;
	byte AlarmMode;
    byte lpEnable;
	byte name[20];
    byte playerNo;
    byte loc[9];
    byte pwrMode;
    byte proxEnable;                                 // Added by Jason Chen, 2014.05.20
    byte mntLoc;
    //byte realXmitEnable;                             // Added by Jason Chen, 2014.03.19    
    //byte calLinOffset[3];
} GFT_PROFILE;


#define ACC_BUFF_CSTAT_FULL_MSK  0x1
#define ACC_BUFF_CSTAT_WRAP_MSK  0x2
typedef struct accBufferDescriptor
{   
   unsigned char cstatus;     // control and status 
   unsigned short length;     // length (number of char)
   ACC_ENTRY *pEntry;         // entry address 
   GYRO_INFO *pGyroInfo;      // gyro data
   ACC_DATA *pData;           // next data address
   void   *next;              // next acc_bd   
} ACC_BD;


typedef struct 
{
	byte stat;
	byte idx;
	MY_RTC t;  
    MOTION_DATA data;
	long flag;
} IM_MOTION_DATA;
#if 1
#define ACC_MAX_BD_NUM  0x3
#else
#define ACC_MAX_BD_NUM  0x3
#endif

#define IM_SLEEP_TIMER       16 //15    //3 //seconds      // Changed from 15 to 5 by Jason Chen, 20140106
#define IM_HIBERNATE_TIMER   60*30  //30 minutes

//extern byte accInitFinished;

extern word imSleepTimer;
extern word imSleepTimerTime;

extern GFT_PROFILE usrProfile;


#define ACC_HIT_DATA_PRE 0x1
#define ACC_HIT_DATA_PST 0x2

#define ACC_HIT_STAT_INIT   0x0
#define ACC_HIT_STAT_PRE    0x1
#define ACC_HIT_STAT_PST_S  0x2
#define ACC_HIT_STAT_PST    0x3
#define ACC_HIT_STAT_WAIT   0x4
#define ACC_HIT_STAT_END    0x5

extern IMPACT_STATE imState;
extern byte accHitStat;
extern volatile byte accLive;
extern byte GID[6];
extern ACC_BD *pAccNowR;
extern ACC_BD *pAccNowW;
extern  volatile byte accHitOn;
extern uint32_t gActiveTime;

extern IM_MOTION_DATA imSummaryData;

extern long totalSecond;

//extern MY_RTC lastTime;        // Added by Jason, 2013.12.18

//extern WORD accMaxValue;
//extern  byte accAlarmHold;
#define     accGetAbs(x) (x > 128 ? (x-128)*(x-128): (128-x)*(128-x))
#define     accGetSum(x,y,z) (abs(x)+abs(y)+abs(z))
//#define 	accMax(a, b,c)   (accGetAbs(a) > accGetAbs(b) ? (a) : (b))
//#define 	accMax3(a, b, c)   (accGetMag(a) > accGetMag(b) ? accMax((a),(c)) : accMax((b),(c)))

#define accDisable() ( ACC_INT_RX_DISABLE(); ACC_INT_TX_DISABLE();)	

//byte AccSpiWriteRead(MMA68_SPI_CMD *cmd, MMA68_SPI_RSP *rsp);


#define     accGetSum16(x,y,z) (abs(x)+abs(y)+abs(z))


#define accGetResult(x,y,z) ( (x)*(x)+(y)*(y) +(z)*(z))

int accInitBDs(void);



//byte AccGetStat(byte axis,byte *data);
//byte AccSoftReset(void);


//byte AccWriteReg(byte reg, byte data, byte needValidate);
//byte AccReadReg(byte reg, byte *data);
//byte AccReadData(ACC_DATA *pAcc);
void spiFlashCreateTimeStampEntry(byte mode);

void imSaveDataItemToEntry(byte type);
void imUpdateBucket(ACC_DATA data, byte over10);
void imRstBucket(void);

void imTask(void);
#endif

