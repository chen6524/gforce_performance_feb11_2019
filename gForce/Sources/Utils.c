/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "gft.h"
#include <hidef.h>
#include "rtc.h"
#include "Utils.h"

void mTimer_TimeSet (unsigned int *pTimer ) 
{     
     
     DisableInterrupts;
     *pTimer = g_TickSecond;
     EnableInterrupts;        
}

unsigned char mTimer_TimeExpired( unsigned int *pTimer, unsigned int time  ) 
{      
     unsigned int tmpTimer;
     DisableInterrupts;
     tmpTimer = g_TickSecond;
     EnableInterrupts; 
    return ((tmpTimer -*pTimer >= time) ? 1 : 0);	
}


#if 0  // for comparing purpose, 2014.06.24
void CYFISNP_NODE_TimeSet1(WORD *pTimer, WORD time)               // 32767 counts maximum    
{
    WORD tmpTime;
    
    tmpTime = SleepTimer_TickCount;
    
    *pTimer = (WORD)(tmpTime + time);
    if( *pTimer == 0 )
        *pTimer = 1;
}

BOOL CYFISNP_NODE_TimeExpired1(WORD *pTimer) 
{
    WORD tmpTime;
    if( *pTimer == 0 )
        return TRUE;

    tmpTime = SleepTimer_TickCount;
    tmpTime = (WORD)(*pTimer - tmpTime);
    
    if (((BYTE)(tmpTime>>8)&0x80) == 0)
        return FALSE;   // not expired

    *pTimer = 0;
    return TRUE;    // expired
}
#endif


//////////////////////////////////
unsigned int CRC_Value;

void CalculateCRC(unsigned char * Buf, unsigned char Len)
{
 	unsigned char Index1, Index2;
  
	CRC_Value = PRESET_VALUE;
    
	for (Index1=0; Index1 < Len; Index1 ++)
	{
			CRC_Value = CRC_Value ^ (unsigned int) Buf[Index1];
			
			for (Index2 = 0; Index2 < 8; Index2++)
			{
					if (CRC_Value  & 0x0001)
						CRC_Value = (CRC_Value >> 1) ^ POLYNOM;
					else
						CRC_Value = (CRC_Value >> 1);
			}
	}
	
	//The last calculated value
	CRC_Value = ~ CRC_Value;
}

void MakeCRC(unsigned char * Buf, unsigned char Len)
{
  CalculateCRC(Buf, Len - 2);
  
  Buf[Len-2] = CRC_Value & 0xFF;
  Buf[Len-1] = (CRC_Value >> 8) & 0xFF;
}

unsigned char VerifyCRC(unsigned char * Buf, unsigned char Len)
{
  CalculateCRC(Buf, Len);
  return CRC_Value == ~CHECK_VALUE;
}
byte CalcCRC8(byte* data_p, byte length)                       // Added by Jason Chen, 2016.03.29
{

  byte i,j;
  byte data = 0;
  
  for(j = 0; j< length; j++) 
  {
    data = data ^ *data_p++;
    for ( i = 0; i < 8; i++ )
    {
        if (( data & 0x80 ) != 0 )
        {
            data <<= 1;
            data ^= 0x07;        // Poly = 0x07;
        }
        else
        {
            data <<= 1;
        }
    }   
  } 
  
  return data; 
}

#if 0
unsigned long ISqrt(unsigned long lv)
{
    unsigned long lr = lv >> 1;
    unsigned char n;

    for(n=32; n>0; n--)
    {
        if((lv & (1 << (n-1))) != 0)
        {
            break;
        }
    }

    lr = 1 << (n / 2 + 1);
    
    if(lr == 0) lr = 1;
    
    for(n=0; n<16; n++)
    {
        if(lv == lr * lr)
        {
            break;
        }
        
        if(lr != 0)
        {
            lr = (lr + lv / lr) / 2;
        }
    }
    
    return lr;
}
#endif
/* From wikipedia
if year modulo 400 is 0 then leap
 else if year modulo 100 is 0 then no_leap
 else if year modulo 4 is 0 then leap
 else no_leap
*/
static unsigned char IsLeapYear(unsigned char y)
{
    unsigned int ty;
    
    ty = 2000 + y;
    
    if((ty % 400) == 0)
    {
        return TRUE;
    }
    else if((ty % 100) == 0)
    {
        return FALSE;
    }
    else if((ty % 4) == 0)
    {
        return TRUE;
    }

    return FALSE;
}

const unsigned char DaysPerMonth[]=
{ // 1   2   3   4   5   6   7   8   9  10  11  12
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
};

unsigned char GetTotalDaysInMonth(unsigned char ThisYear, unsigned char ThisMonth)
{

    if(IsLeapYear(ThisYear) && (ThisMonth == 2))
    {
        return 29;
    }

    return DaysPerMonth[ThisMonth - 1];
}


unsigned char IsValidDateTime(unsigned char year, unsigned char month, unsigned char day, 
                              unsigned char hour, unsigned char minute, unsigned char second)
{
   
   
    if( (year <100) &&   (month <13) &&  (day <= GetTotalDaysInMonth(year, month)) &&   (hour<25)
        && ( (minute <60)   && (second <60)))
        return TRUE;
    else 
        return FALSE;
}

void initBuzz(void)
{
}


//assume data get upload minumum one month
long diffTime(MY_RTC *sRTC, MY_RTC *eRTC)
{
	long mT1 =0;
	long mT2 =0;
	
	if(eRTC->Month != sRTC->Month) 
	{
		mT1 = GetTotalDaysInMonth(sRTC->Year, sRTC->Month)-eRTC->Day + eRTC->Day;
		
	}
	else 
		mT1 = eRTC->Day - sRTC->Day;

	mT2 = eRTC->Hour - sRTC->Hour;
	

	mT2 =mT1*24 + mT2;
    
	mT1 =eRTC->Minute -sRTC->Minute;
    
	mT2 = mT2*60 + mT1; //minutes
	
	mT1 = eRTC->Second - sRTC->Second;
	mT2 = mT2 * 6 + mT1/10;
    return mT2;

}

void rtcAdjust(MY_RTC *mRTC,unsigned char mSecond)
{
 
 byte tmpS =0;
 byte tmp =0;
 byte tmpM =0;
 byte s =0;
 byte second;
 second  = mSecond&0x7F;

 	if( mSecond < 128)
 	{  
 	    
 	    tmpS = mRTC->Second+second;
		tmp = tmpS%60;
		tmpM = tmpS/60;
		mRTC->Second = tmp;//tmpS-tmp*60;
		tmpM = mRTC->Minute +tmpM;
		
        if(tmpM >=60)
        {
			mRTC->Minute = tmpM -60;

			if(mRTC->Hour < 24)
			{
				mRTC->Hour++;


			}
			else
			{
				
				mRTC->Hour = 0;
			    if(GetTotalDaysInMonth(mRTC->Year, mRTC->Month)  > mRTC->Day)
					mRTC->Day++;
				else 
				{
				    mRTC->Day = 1;
					if(mRTC->Month <12)
						mRTC->Month++;
					else
					{
						mRTC->Month = 1;
						mRTC->Year++;
					}
				}
			}
					
        }
		else
		{
				mRTC->Minute = tmpM;

		}
        
 	}
 else
 {
	 
	 
	 tmpM = tmpS%60;
	 tmpS = second - tmp *60;
	 if(tmpS > mRTC->Second)
	 	{
	     tmpM++;
		 mRTC->Second = mRTC->Second +60 - tmpS;
	 	}
	 else 
	 	mRTC->Second  = mRTC->Second -tmpS;
	 
	 
	 
	 if(mRTC->Minute >= tmpM)
	 {
		mRTC->Minute -=tmpM;

	 }
	 
	 else
	 {
		 mRTC->Minute = mRTC->Minute + 60 - tmpM;
	 
		 if(mRTC->Hour >= 1)
		 {
			 mRTC->Hour--;
	 
		 }
		 else
		 {
			 
			 mRTC->Hour = 23;
			 if( mRTC->Day > 1)
				mRTC->Day--;
			 else 
			 {
			     if(mRTC->Month >1)
			     {
			     	s = GetTotalDaysInMonth(mRTC->Year, mRTC->Month-1);
				 	mRTC->Day = s;

			     }
				 else
				 	{
				 	mRTC->Year--;
					mRTC->Month =12;
				 	}
				 
				
			 }
		 }
				 
	 }
	 
 }

                


}

