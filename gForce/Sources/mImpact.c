/********************************************************************************
*  
* High G accelerometer 
* History:
*
* 11/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include "gft.h"
#include "mcu.h"
#include "LSM330DLC.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mFlash.h"
#include "mHid.h"
#include "rtc.h"
#include "utils.h"
#include "gLp.h"
//Faraz
#include "radio.h"
#include "MAX44000.h"
#include "max14676.h"
#include <stdlib.h>
#include "bcd_common.h"                        // Added by Jason Chen for time stamp fix, 2013.12.19
#include "kbi.h"


#define ACC_RAM_STATE_FULL 0x1
#define ACC_RAM_STATE_CURBUF_HASDATA 0x2
#define ACC_RAM_STATE_CURBUF_FULL 0x4

static byte accRamState =0;
//test
//word testMsS = 0;
//word testMsE = 0;

static ACC_BD accBD[ACC_MAX_BD_NUM];
ACC_ENTRY accBuff[ACC_MAX_BD_NUM]; 

byte accHitStat = ACC_HIT_STAT_INIT;
byte accHitCnt =0;
byte accHitPreCur =0;
byte accHitPreTotal = 0;
ACC_BD *pAccNowR;
ACC_BD *pAccNowW;
//WORD accMaxValue;
volatile byte accHitOn =0;

GFT_PROFILE usrProfile;

extern byte field_xmit_enable;      // Added by Jason Chen for Recording Control, 2014.08.05

IM_MOTION_DATA imSummaryData;

long AccCalc(ACC_ENTRY *entry);

void SetUsrProfileLP_Disable(void)                     // Added by Jason Chen, 2019.02.12
{
   LED_BLUE_On();
   Cpu_Delay100US(5000);                               
   usrProfile.lpEnable = GMODE_LP_DISABLE;    
   needSaveProfile     = 1;					                      
   needReboot          = 1;                                
}

void imRstBucket(void)
{
  int i;
  for (i =0;i<12;i++)
  	 pAccNowW->pEntry->bucket.bucket[i] = 0; 
  gftInfo.explosiveFlag = 0;

}

int accInitBDs(void)
{
	byte i;
	for ( i = 0; i< ACC_MAX_BD_NUM; i++)
	{
	    accBD[i].pEntry = &accBuff[i];
	    accBD[i].next =   (void *)&accBD[i+1];
		accBD[i].length = 0; 
		accBD[i].cstatus = 0;
		//accBD[i].pEntry->offset.size =0;
		accBD[i].pEntry->maxIndex =0;
		accBD[i].pGyroInfo = &gyroInfo[i];
	}
	accBD[ACC_MAX_BD_NUM-1].next = (void *)&accBD[0];
	
	pAccNowR = pAccNowW = &accBD[0];
	
	//accMaxValue._word = 0;
	imRstBucket();
	return(E_OK);
} 

uint32_t gActiveTime;

// Changed by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME \
		   if(motionDetected)gActiveTime++; 
		   
// Added by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME2 \
		   gActiveTime++; 	

#define   RPE_COE_0    1
#define   RPE_COE_1    2
#define   RPE_COE_2    3
#define   RPE_COE_3    5

#define   RPE_COE_4    7
#define   RPE_COE_5    7
#define   RPE_COE_6    9
#define   RPE_COE_7    9
#define   RPE_COE_8    9
#define   RPE_COE_9    10
#define   RPE_COE_10   10
#define   RPE_COE_11   10


const byte rpeCoe[10] = {RPE_COE_1,RPE_COE_2,RPE_COE_3,RPE_COE_4,RPE_COE_5,RPE_COE_6,RPE_COE_7,RPE_COE_8,RPE_COE_9,RPE_COE_10};
long totalSecond =0;

void imUpdateRpe(void)
{
    int i;
	long j =0;
	long rpe = 0;
//#define USE_OLD_PER 1
#if USE_OLD_PER 



		for(i =0;i<10;i++)
		{
			 
			rpe += pAccNowR->pEntry->bucket.bucket[i] * rpeCoe[i];
			
		}
		
		rpe = rpe/100 ;
		
		gftInfo.load += rpe; 
		totalSecond +=10;
		gftInfo.rpe = gftInfo.load/ (totalSecond/10);
#else
		for(i =0;i<10;i++)
		{
			 
			rpe += pAccNowR->pEntry->bucket.bucket[i] * rpeCoe[i];
			
		}
			
		//j= 1000 - pAccNowR->pEntry->below_1_5G;
		//rpe =10* rpe/j ;
		gftInfo.rpeLoad += rpe;  
		totalSecond += 1000; // j 10ms
		gftInfo.rpe = 10*gftInfo.rpeLoad/(totalSecond);
		gftInfo.load += gftInfo.rpe;
#endif

    
	
	
}
void imUpdateBucket(ACC_DATA data, byte over10)
{
    byte i =0;
	
	int32_t r;
	if(over10)
	{
		
		r  = B_10G; // make one value

	}
	else
		r =(int32_t)accGetResult((int32_t) data.x>>2,(int32_t)data.y>>2,(int32_t)data.z>>2);
    if((r > (int32_t)B_2G))
	{
	  gftInfo.explosiveFlag ++; 
	}
	else
	{
	  if(gftInfo.explosiveFlag  >=50 )
	  {
	    gftInfo.expCnt++;
		gftInfo.expPwr += gftInfo.explosiveFlag;
		
	  	(pAccNowW->pEntry->bucket.bucket[10])++;
		(pAccNowW->pEntry->bucket.bucket[11]) += gftInfo.explosiveFlag;

	  }
	  
	  gftInfo.explosiveFlag  = 0;  
	}
    
	
	if(over10)
		(pAccNowW->pEntry->bucket.bucket[9])++; 
	//else if(r < (int32_t)B_1G)
	//{
	//  i =10;
	//  (pAccNowW->pEntry->bucket.bucket[i])++; 
	//}
	//else if(r < (int32_t)B_1G+ B_1G_OFFSET && r > (int32_t)B_1G -B_1G_OFFSET)
	//{
	//  i =11;
	//  (pAccNowW->pEntry->bucket.bucket[i])++; 
	//}	
	else if(r < (int32_t)B_2G)
	{
	  i =0;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	  
	}
	else if(r < (int32_t)B_3G)
	{
	  i = 1;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_4G)
	{
	  i = 2;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_5G)
	{
	  i = 3;
	 (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_6G)
	{
	  i = 4;
	 (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_7G)
	{
	  i = 5;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r <(int32_t)B_8G)
	{
	  i = 6;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_9G)
	{
	//LED_YELLOW_On();
	  i = 7;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else if(r < B_10G)
	{
	 //LED_YELLOW_On();
	  i = 8;
	  (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
	else  
	{
	  i = 9;
	 (pAccNowW->pEntry->bucket.bucket[i])++; 
	}
		

}

void imSaveDataItemToEntry(byte type)
{
	volatile byte rtcV =0;   
	static volatile uint32_t mFlag =0;
	if(accRamState & ACC_RAM_STATE_FULL)
	{        
		LED_RED_On();
		return;  // out of ram  and get out of here  	 
	}
  	                            
    if (usrProfile.proxEnable) 
    {  
     //if(!ProxIntState)  
       if((!ProxIntState)||(!field_xmit_enable))           // Added by Jason Chen,   2014.08.05     
       {
        accHitStat = ACC_HIT_STAT_INIT;
        return;
       }
    } 
    else                                                  // Added by Jason Chen,   2014.08.05
    {                                                     // 
        if(!field_xmit_enable)                            // 
        {                                                 // 
          accHitStat = ACC_HIT_STAT_INIT;                 // 
          return;                                         // 
        }                                                 // 
    }                                                     // Added by Jason Chen,   2014.08.05
  	                                           
	switch(accHitStat)
	{
	case ACC_HIT_STAT_PRE:
		if(accHitCnt <= ACC_HIT_CNT_MAX-1)
		{	 
		  //motionDetected =0;                              	                                                          
		  pAccNowW->pEntry->rtcAcc.data[accHitCnt].accData = accDataM;
		  pAccNowW->pEntry->rtcAcc.data[accHitCnt].gyroData = gyroDataM;   

		  if(type)
		  {
		       
#if 0
			    if( imSummaryData.flag ==0)
			    {
					imSummaryData.flag = (long)1<<accHitCnt;
					imSummaryData.data = pAccNowW->pEntry->rtcAcc.data[accHitCnt];	   
					(void)memcpy((void *)&imSummaryData.t, (void *)&pAccNowW->pEntry->t, sizeof(MY_RTC));	        
			    }
				else
#endif
				//pAccNowW->pEntry->iFlag |= (long)1<<accHitCnt;	
					
				if((gftInfo.lpBusy != TRUE) && (lpImData.lpAccDataMValid !=1)) //lpState != LP_STATE_SUMMARY_IM)  
				{
				    
					lpCmdMail = LP_CMD_SUMMARY;	 
					lpImData.lpAccDataM= accDataM;
					lpImData.lpGyroDataM = gyroDataM;
					lpImData.lpAccDataMValid =1;
					lpImData.index = accHitCnt;
					(void)memcpy((void *)&lpImData.rtc, (void *)&myRTC, sizeof(MY_RTC));
					lpImData.rtc.StartMS = g_Tick1mS;
					

				}
				else
				    mFlag  |= (long)1<<accHitCnt;	
				
		  }
		  accHitCnt++;
		  if(accHitCnt > ACC_HIT_CNT_MAX-1)
		  	accHitStat = ACC_HIT_STAT_PST; 
		  else
		  	break;
		}
		else	      
		  accHitStat = ACC_HIT_STAT_PST; 
		
    case ACC_HIT_STAT_PST:    		
  		accHitStat = ACC_HIT_STAT_END;
  		pAccNowW->pEntry->magic = ACC_GYRO_MAGIC;
  	    	  
		
	//break;
    case ACC_HIT_STAT_END:
		 pAccNowW->pEntry->iFlag = mFlag;
	     pAccNowW->cstatus =  ACC_BUFF_CSTAT_FULL_MSK;
	     pAccNowW = (ACC_BD *)pAccNowW->next;

	     if( pAccNowW->cstatus & ACC_BUFF_CSTAT_FULL_MSK)
	     {
		     accRamState = ACC_RAM_STATE_FULL;
			 break;
	     }
	     accHitStat = ACC_HIT_STAT_INIT;
		 
		 if(motionDetected)
			motionDetected = 0;
	
	    
    case ACC_HIT_STAT_INIT:  
		
        (void)memset(pAccNowW->pEntry->rtcAcc.data, 0x00, ACC_HIT_DATA_LEN*sizeof(MOTION_DATA));  
		pAccNowW->pEntry->iFlag = (long)0;
		accHitStat = ACC_HIT_STAT_PRE;	     
		accHitCnt =0; 
		mFlag =0;
        //(void)memcpy((void *)&lpImData.rtc, (void *)&myRTC, sizeof(MY_RTC));	  
		(void)memcpy((void *)&pAccNowW->pEntry->t, (void *)&myRTC, sizeof(MY_RTC));	  
         pAccNowW->pEntry->t.StartMS = g_Tick1mS;
		//lpImData.rtc.StartMS = g_Tick1mS;
		imRstBucket();
		
		 
	break;    
  }

}



//ACC_DATA tmpBuffer[ACC_HIT_PRE_CNT];

#if 0
static word AccFindRow(ACC_ENTRY *entry)
{

	byte row;
	byte i,j,c;
	word tMax;
	word tmp;
	row =0;
	tMax = 0;
	j = entry->offset.size;
	c = ACC_HIT_PRE_CNT - j;
	
	if (j  == 0)
		//(void)memcpy((void *)tmpBuffer, (void *)entry->data, ACC_HIT_PRE_CNT* sizeof(ACC_DATA));
	  (void)memcpy((void *)tmpBuffer, (void *)entry->rtcAcc.data, ACC_HIT_PRE_CNT* sizeof(ACC_DATA));   // Changed by Jason Chen, 2015.02.06
		
	else
	{
	#if 0
		(void)memcpy((void *)&tmpBuffer[c], (void *)entry->data, (ACC_HIT_PRE_CNT - c) * sizeof(ACC_DATA));
		(void)memcpy((void *)tmpBuffer, (void *)&entry->data[j], c* sizeof(ACC_DATA));
	#else	                                                                                                        // Changed by Jason Chen, 2015.02.06
		(void)memcpy((void *)&tmpBuffer[c], (void *)entry->rtcAcc.data, (ACC_HIT_PRE_CNT - c) * sizeof(ACC_DATA));
		(void)memcpy((void *)tmpBuffer, (void *)&entry->rtcAcc.data[j], c* sizeof(ACC_DATA));
	#endif
		
	}
	
	//(void)memcpy((void *)entry->data, (void *)tmpBuffer, ACC_HIT_PRE_CNT * sizeof(ACC_DATA));
	(void)memcpy((void *)entry->rtcAcc.data, (void *)tmpBuffer, ACC_HIT_PRE_CNT * sizeof(ACC_DATA));       // Changed by Jason Chen, 2015.02.06
	
	for ( i =0; i< ACC_HIT_DATA_LEN; i++)
	{
        //tmp = accGetSum(entry->data[i].x,entry->data[i].y,entry->data[i].z);
        tmp = accGetSum(entry->rtcAcc.data[i].x,entry->rtcAcc.data[i].y,entry->rtcAcc.data[i].z);       // Changed by Jason Chen, 2015.02.06
        if (tmp >= tMax) 
        {
			row = i;
			tMax = tmp;
        }
	}
	entry->maxIndex = row;
	return tMax;

}
#else
static long AccFindRow(ACC_ENTRY *entry)
{

	byte row;
	byte i,j,c;
	long tMax;
	long tmp;
	row =0;
	tMax = 0;
   
	for ( i =0; i< ACC_HIT_DATA_LEN; i++)
	{
        //tmp = accGetSum(entry->data[i].x,entry->data[i].y,entry->data[i].z);
        //row = i;
        tmp = (long)accGetResult((entry->rtcAcc.data[i].accData.x),(entry->rtcAcc.data[i].accData.y),(entry->rtcAcc.data[i].accData.z));
        //tmp = accGetSum(entry->rtcAcc.data[i].accData.x,entry->rtcAcc.data[i].accData.y,entry->rtcAcc.data[i].accData.z);       // Changed by Jason Chen, 2015.02.06
        if (tmp >= tMax) 
        {
			row = i;
			tMax = tmp;
        }
	}
	entry->maxIndex = row;
	//tMax = accGetResult((int32_t)(entry->rtcAcc.data[row].accData.x),(int32_t)entry->rtcAcc.data[row].accData.y,(int32_t)entry->rtcAcc.data[row].accData.z);
	return tMax;

}

#endif
long AccCalc(ACC_ENTRY *entry)
{
	long acc;
	ACC_DATA *xyz;
	//byte i;

	acc = AccFindRow(entry);
	entry->mag = acc;
	
	return acc;
}

IMPACT_STATE imState=IM_STATE_SOFT_RESET;
volatile byte myTest;
volatile byte accLive =0;

long acc2;
byte field_xmit_enable = 1;           // Added by Jason Chen for the control of field xmit, 2014.03.19
extern uint8_t  max17047Exist;        // Added by Jason Chen for, 2014.04.22
extern uint16_t max17047Voltage;      // Added by Jason Chen for, 2014.04.22

word gSleepCnt =0; 
//Faraz
#define USB_OFF       0
#define USB_ON        1
//extern bool Usb_Mode;                    // Commented by Jason Chen because don't need it in this module, 2013.12.13
extern byte Message_Registered;
                                            

word imSleepTimer = 0;
//word imSleepSendTimer = 0;

word imSleepTimerTime = IM_SLEEP_TIMER;
byte lpTxEnable = 0;
byte timeA; 
byte timeB;
void imTask(void)
{
    //Faraz 
    byte Return_Value = 0;
    dword wTemp;
    static byte waitLoop = 0;
	//static ACC_STATE lowPriState = ACC_STATE_NONE;
	long accT = 0;
	byte alarmCoefficent =100;  
	//byte i;
    //
  switch(imState)
  {
	  case IM_STATE_SOFT_RESET:
		    imState=IM_STATE_INIT;
		    break;
	  case IM_STATE_INIT:
            if(usrProfile.mntLoc <2 || usrProfile.mntLoc >=100)
				alarmCoefficent =100;
			else 
				alarmCoefficent = usrProfile.mntLoc;
			
		    wTemp = usrProfile.tAlarm*100;
			wTemp = wTemp/alarmCoefficent;
		    //acc2 = (word)(wTemp * wTemp * ACC_RESOLUTION_SQR);
		    //acc2 = (long)(wTemp * wTemp * 6944);
			//acc2 = (long)(wTemp * wTemp * ACC_RESOLUTION_SQR);//((MMA68_G_LSB*4)*(MMA68_G_LSB*4));
			acc2 = (long)(wTemp * wTemp * 16393);//((MMA68_G_LSB*4)*(MMA68_G_LSB*4));
			//acc2 = 70000000;
		    (void)spiFlashGetCurWtAddr();
		    imState = IM_STATE_WAIT;
		    break;
	  case IM_STATE_WAIT:
		    if( waitLoop++ > WAIT_AFTER_RESET)
		    {			
			     imState=IM_STATE_IDLE;
		    }
		    break;		
	  case IM_STATE_IDLE:	
        if(!gftInfo.pwrMode) 
        { 
            mTimer_TimeSet(&imSleepTimer );
            
        }
        imState=IM_STATE_SLEEP_WAIT;  
    case IM_STATE_SLEEP_WAIT:
		
        if((pAccNowR->cstatus & ACC_BUFF_CSTAT_FULL_MSK))
		{	 
		
			     imState=IM_STATE_CALC;
				 
				  
		}                                        
        else if(!gftInfo.pwrMode) 
        { 
                     
            if(mTimer_TimeExpired( &imSleepTimer, imSleepTimerTime ) )
            {                     
               
                
#if ENABLE_SLEEP_PACKET_SEND                
               mTimer_TimeSet( &imSleepTimer );                                           // Added by Jason Chen, 2014.01.15 for Sending enter Sleep mode MSG
                                   
               Power_Off_Message.gid[0] = 0x01;                                           // 0x01 means Sleep Packet, added by Jason Chen, 2014.01.15                                                     
               Power_Off_Message.gid[1] = max17047Exist|max14676Exist;                                  // added battery level value into the special Packet, 20140314   
               Power_Off_Message.gid[2] = (byte)(batData1>>8);                            
               Power_Off_Message.gid[3] = (byte)batData1;
               Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
               Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;                                                
               Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
               Power_Off_Message.gid[7] = (byte)max17047Voltage; 
               
               Power_Off_Message.accEntryCnt = spiFlashCurRdBuffer.accEntryCnt;           // Added by Jason Chen, 2016.01.27                                                               
               Power_Off_Message.batPT =  (byte)max14676Charge;                                                                                                                         
               lpCmdMail = LP_CMD_OFF;                // 0x57 for testing, 20140115             
               lpState   = LP_STATE_IDLE;                                        
               imState   =IM_STATE_SENDING_SLEEP_MSG;                      
#else
               imState = IM_STATE_SLEEP;
#endif                   
                                            
            }
            else
            {
                if(IRQSC_IRQF)
                {
                    mTimer_TimeSet( &imSleepTimer );                    
                    IRQSC_IRQACK = 1;
					motionDetected =2;
					pwrOffCnt =0;
                }
            }
			
        }
       
	    break;
	    
#if ENABLE_SLEEP_PACKET_SEND	      
    case IM_STATE_SENDING_SLEEP_MSG:
        
        if(mTimer_TimeExpired( &imSleepTimer, 3 ))     // Added by Jason Chen, 2014.01.15 for Sending "enter Sleep mode" Packet 
        {
           //if(!gftInfo.lpBusy ) 
           {            
    		     imState = IM_STATE_SLEEP;                          
    		     buzzerOff();                              // Added by Jason Chen, 2014.04.14
           }
        } 
        else 
        {
            if(IRQSC_IRQF)                             // Added by Jason Chen, 2014.03.20, not enter Sleep mode MSG
            {
                mTimer_TimeSet( &imSleepTimer );
                IRQSC_IRQACK = 1;
                
                imState=IM_STATE_SLEEP_WAIT;  
                
                Power_Off_Message.gid[0] = 0x80;       // New Battery level report, 2014.03.20
                lpCmdMail = LP_CMD_OFF;
                lpState=LP_STATE_IDLE;   
				motionDetected =2;
            }            
        }
                             
        break;
#endif                        
    case IM_STATE_SLEEP:				
#if POWER_ON_BAT_REPORT                           // Added by Jason Chen for sending Power-On Packet periodically, 2013.11.26
        lpCmdMail = LP_CMD_PWR_ON;                     
        lpState   = LP_STATE_IDLE;          
          
        imState=IM_STATE_IDLE;
#else
		    DisableInterrupts;
		    pAccNowR->cstatus = 0;
		    EnableInterrupts; 
			timeA = myRTC.Second;
            ResetADCTimer(); 		          
		    gotoStop3();  
			
			(void)getRtcStamp();	
			//if(myRTC.Second > timeA)
			 // totalSecond += myRTC.Second - timeA;
			//motionDetected = 2;
      //reset previous recording 
        imState=IM_STATE_IDLE;  
	    accHitStat =  ACC_HIT_STAT_INIT;   
#endif		             
	  break;
	  case IM_STATE_CALC:	
		    __RESET_WATCHDOG();
            if(pAccNowR->pEntry->iFlag!=0)
            	accT = AccCalc(pAccNowR->pEntry);
			else 
				accT = 0;
			
		    //(void)gyroCalc();
		    imUpdateRpe();
			
			#if 1
			
		    if( (usrProfile.AlarmMode & PROFILE_ALARM_EN_MSK) && acc2 <= accT )
		    {
		        if(profile_alarm_au_backup & PROFILE_ALARM_AU_MSK)
		    
		        //accHitOn =1; //turn on alarm   
		        usrProfile.AlarmMode |= PROFILE_ALARM_ON_MSK;
		        if(usrProfile.AlarmMode & PROFILE_ALARM_LOCK_MSK)
		  	      needSaveProfile = 1;     // Added by Jason Chen, 2014.11.26
		        //usrProfile.AlarmMode |= PROFILE_ALARM_AU_MSK;		     
		      //accMaxValue._word = accT;
#ifdef  ENABLE_RADIO               
            (void)memcpy((byte *)&newRTC, (void *)&myRTC, sizeof(MY_RTC));   // Added by Jason Chen, 2015.02.06
#endif        	
	      }	
#endif


		   imState=IM_STATE_SAVE;	
		   break;
	  case IM_STATE_SAVE: //take 0.6ms/256 bytes
        // save data to flash
        // 
        __RESET_WATCHDOG();  
        LED_YELLOW_On();
		
        (void)spiFlashWriteAccBuffer(pAccNowR->pEntry);
	    LED_YELLOW_Off();
	    DisableInterrupts;
	    pAccNowR->cstatus =0;
        pAccNowR->pEntry->magic =0;
	    accRamState =0;
	    EnableInterrupts; 
	    pAccNowR = pAccNowR->next;
	    imState=IM_STATE_IDLE;
		
        spiMyFlashGetCurWtAddr();                                                            // Added by Jason for Summary Impact Count sent out, 2015.01.08
		//update session info
		  		  
#if 1
	     ///////////////////////////////////////////////// Added by Jason for Summary sent out, 2015.01.08	
	    //if(usrProfile.AlarmMode & PROFILE_ALARM_ON_MSK)
	    if(!gftInfo.lpBusy)
		    lpCmdMail = LP_CMD_BUCKET;  
	    	
        
	    	
		
        //lpCmdMail = LP_CMD_RPT;
       /////////////////////////////////////////////////
#endif	      	      
		  
		  
      break;
	  default:
		    imState=IM_STATE_INIT;
      break;
  }	
}
