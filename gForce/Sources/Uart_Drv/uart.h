
#ifdef ENABLE_LP
#ifndef __UART_H
#define __UART_H

#define UART_COMM_BUFF_SIZE 31

typedef struct
{
	volatile byte TX[UART_COMM_BUFF_SIZE];
	volatile byte RX[UART_COMM_BUFF_SIZE];
	
	volatile byte TXp;
	volatile byte TXg;
	volatile byte RXp;
	volatile byte RXg;
	
	volatile byte TXf;
	volatile byte RXf;
	
	volatile byte TXl;
	volatile byte RXl;
} UartParams;

void uartInit(void);

void uartTask(void);

typedef enum 
{
	UART_STATE_ALARM,
    UART_STATE_INIT,
    UART_STATE_RE_TX,
    UART_STATE_IDLE,
    UART_STATE_TX, 
    UART_STATE_RX,
    UART_STATE_RPT,
    UART_STATE_ON,
    UART_STATE_OFF
}UART_STATE;


/*
+---+---------------+--------------------------------------------------------+ 
|   |  7    -    0                                                           |
|---+---------------+--------------------------------------------------------+
| 1 | cmd                                                                  |
|---+---------------+--------------------------------------------------------+
| 2 | sub cmd                                                             |
|---+---------------+--------------------------------------------------------+
| 3 | data                                                            | 

*/
#define UART_PROTO_CMD_MSK 0x80
#define UART_PROTO_LEN_MSK  0x7F

typedef enum 
{
	UART_CMD_NONE,
	UART_CMD_ON,
    UART_CMD_OFF,
    UART_CMD_ALARM, 
    UART_CMD_RPT 
    
}UART_CMD;
#define UART_PROTO_PKT_HEAD_LEN 0x5
typedef struct 
{   
	byte magic[2]; // 
	word len;   // data len
	byte cmd;   // 
	byte data[400];    
} UART_PROTO_PKT; 

#define UART_MAX_PKT_LEN 14

extern volatile byte uartCmdMail;

#endif

#endif //enable lp
